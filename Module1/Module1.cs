﻿using Module1;
using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Entity e = new Entity();
            Console.Write("Введите число a: ");
            e.a = int.Parse(Console.ReadLine());
            Console.Write("Введите число b: ");
            e.b = int.Parse(Console.ReadLine());
            Module1 m = new Module1();
            int[] swapItems = m.SwapItems(e.a, e.b);
            Console.WriteLine("Число a: " + swapItems[0]);
            Console.WriteLine("Число b: " + swapItems[1]);
            Console.Write("Введите размер массива: ");
            int length = Int32.Parse(Console.ReadLine());
            int[] arr = new int[length];
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write($"Введите {i} элемент: ");
                arr[i] = int.Parse(Console.ReadLine());
            }
            int min = m.GetMinimumValue(arr);
            Console.WriteLine($"Минимальный элемент одномерного массива: {min} ");
        }


        public int[] SwapItems(int a, int b)
        {
            int temp = a;
            a = b;
            b = temp;
            int[] arr = { a, b };
            return arr;
        }

        public int GetMinimumValue(int[] input)
        {
            int min = input[0];
            for (int i = 1; i < input.Length; i++)
            {
                if (min > input[i])
                {
                    min = input[i];
                }
            }
            return min;
        }
    }
}
